import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import PokemonListPage from './components/pokemon-list/PokemonList';

const App: React.FC = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<PokemonListPage />} />
      </Routes>
    </Router>
  );
};

export default App;

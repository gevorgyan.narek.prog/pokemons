import React, { useState } from 'react';
import { Box, Button, MenuItem, TextField } from '@mui/material';

interface PokemonFilterProps {
  onApplyFilter: (filters: PokemonFilters) => void;
}

export interface PokemonFilters {
  name: string;
  type: string;
  ability: string;
}

const PokemonFilter: React.FC<PokemonFilterProps> = ({ onApplyFilter }) => {
  const [nameFilter, setNameFilter] = useState('');
  const [typeFilter, setTypeFilter] = useState('');
  const [abilityFilter, setAbilityFilter] = useState('');

  const handleApplyFilter = () => {
    const filters: PokemonFilters = {
      name: nameFilter,
      type: typeFilter,
      ability: abilityFilter,
    };
    onApplyFilter(filters);
  };

  return (
    <Box display="flex" alignItems="center" marginBottom={2}>
      <TextField
        label="Фильтр по имени"
        variant="outlined"
        value={nameFilter}
        onChange={(e) => setNameFilter(e.target.value)}
        size="small"
        sx={{ marginRight: 2 }}
      />
      <TextField
        label="Фильтр по типу"
        variant="outlined"
        select
        value={typeFilter}
        onChange={(e) => setTypeFilter(e.target.value)}
        size="small"
        sx={{ marginRight: 2 }}
      >
        <MenuItem value="">Все типы</MenuItem>
        <MenuItem value="fire">Огонь</MenuItem>
        <MenuItem value="water">Вода</MenuItem>
        <MenuItem value="grass">Трава</MenuItem>
        {/* Добавьте другие варианты типов */}
      </TextField>
      <TextField
        label="Фильтр по способности"
        variant="outlined"
        select
        value={abilityFilter}
        onChange={(e) => setAbilityFilter(e.target.value)}
        size="small"
        sx={{ marginRight: 2 }}
      >
        <MenuItem value="">Все способности</MenuItem>
        <MenuItem value="ability1">Способность 1</MenuItem>
        <MenuItem value="ability2">Способность 2</MenuItem>
        <MenuItem value="ability3">Способность 3</MenuItem>
        {/* Добавьте другие варианты способностей */}
      </TextField>
      <Button variant="contained" onClick={handleApplyFilter}>
        Применить фильтр
      </Button>
    </Box>
  );
};

export default PokemonFilter;

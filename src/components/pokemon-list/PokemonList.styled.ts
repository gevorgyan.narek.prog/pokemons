import { Card } from '@mui/material';
import { styled } from '@mui/system';

export const AnimatedCard = styled(Card)`
  transition: transform 0.3s ease-in-out;

  &:hover {
    transform: scale(1.05);
  }
`;

export const PokemonCard = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  width: '200px',
  height: '300px',
  padding: '20px',
  borderRadius: '10px',
  boxShadow: '0px 2px 6px rgba(0, 0, 0, 0.1)',
  background: '#4CAF50',
  transition: 'transform 0.3s ease',

  '&:hover': {
    transform: 'scale(1.1)',
  },
});

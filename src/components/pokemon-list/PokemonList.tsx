import React, { useEffect, useState } from 'react';
import { Box, CircularProgress, Typography } from '@mui/material';
import { AnimatedCard, PokemonCard } from './PokemonList.styled';

import PokemonModal from '../pokemon-modal/PokemonModal';
import Pagination from '../pagination/Pagination';
import PokemonFilter, { PokemonFilters } from '../pokemon-filter/PokemonFilter';
import axios from 'axios';

export interface Pokemon {
  id: number;
  name: string;
  avatar: string;
  type: string;
  abilities: string[];
  height: number;
}

export const fetchPokemonImage = async (pokemonId: number) => {
  try {
    const response = await fetch(
      `https://pokeapi.co/api/v2/pokemon-species/${pokemonId}`
    );
    const data = await response.json();
    const variety = data.varieties[0].pokemon.url;
    const varietyResponse = await fetch(variety);
    const varietyData = await varietyResponse.json();
    const imageUrl = varietyData.sprites.front_default;
    return imageUrl;
  } catch (error) {
    console.error('Error fetching Pokemon image:', error);
    return null;
  }
};

const initialFilterParams =  {
  name: '',
  type: '',
  ability: '',
}

const PokemonList = () => {
  const [loading, setLoading] = useState(true);
  const [pokemonList, setPokemonList] = useState<Pokemon[]>([]);
  const [filterParams, setFilterParams] = useState<PokemonFilters>(initialFilterParams);
  const [selectedPokemon, setSelectedPokemon] = useState<Pokemon | null>(null);
  const [paginationParams, setPaginationParams] = useState({
    currentPage: 1,
    totalCount: 20,
    itemsPerPage: 20,
  });

  useEffect(() => {
    const fetchPokemonList = async () => {
      try {
        const response = await axios.get('https://pokeapi.co/api/v2/pokemon', {
          params: {
            limit: paginationParams.itemsPerPage,
            offset:
              (paginationParams.currentPage - 1) *
              paginationParams.itemsPerPage,
            ...filterParams,
          },
        });
        const data = response.data;
        const results = data.results;

        const fetchedPokemonList = await Promise.all(
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            results.map(async (result: any) => {
            const pokemonResponse = await fetch(result.url);
            const pokemonData = await pokemonResponse.json();
            const pokemonId = pokemonData.id;
            const pokemonName = pokemonData.name;
            const imageUrl = await fetchPokemonImage(pokemonId);

            return {
              id: pokemonId,
              name: pokemonName,
              avatar: imageUrl,
              type: pokemonData.type,
              abilities: pokemonData.abilities,
              height: pokemonData.height,
            };
          })
        );
        setPaginationParams((prev) => ({
          ...prev,
          totalCount: data.count,
        }));
        setPokemonList(fetchedPokemonList);
        setLoading(false);
      } catch (error) {
        console.error('Error fetching Pokemon list:', error);
        setLoading(false);
      }
    };

    fetchPokemonList();
  }, [
    paginationParams.itemsPerPage,
    paginationParams.currentPage,
    filterParams,
  ]);

  const handleCardClick = (pokemon: Pokemon) => {
    setSelectedPokemon(pokemon);
  };

  const handleCloseModal = () => {
    setSelectedPokemon(null);
  };

  const handleFilter = (filter: PokemonFilters) => {
    setFilterParams(filter);
  };

  const handlePageChange = (page: number) => {
    setPaginationParams((prev) => ({ ...prev, currentPage: page }));
  };

  const onChangeItemsPerPage = (limit: number) => {
    setPaginationParams((prev) => ({ ...prev, itemsPerPage: limit }));
  };

  return (
    <>
      <PokemonFilter onApplyFilter={handleFilter} />

      {loading ? (
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          height={200}
        >
          <CircularProgress />
        </Box>
      ) : (
        <Box display="flex" flexWrap="wrap" gap={2}>
          {pokemonList?.map((pokemon) => (
            <AnimatedCard
              key={pokemon.id}
              onClick={() => handleCardClick(pokemon)}
            >
              <PokemonCard>
                <img src={pokemon.avatar} alt={pokemon.name} />
                <Typography variant="h5" sx={{ color: '#FFFFFF', mt: 2 }}>
                  {pokemon.name}
                </Typography>
              </PokemonCard>
            </AnimatedCard>
          ))}
        </Box>
      )}

      {selectedPokemon && (
        <PokemonModal pokemon={selectedPokemon} onClose={handleCloseModal} />
      )}

      <Pagination
        itemsPerPage={paginationParams.itemsPerPage}
        currentPage={paginationParams.currentPage}
        totalCount={paginationParams.totalCount}
        onChangePage={handlePageChange}
        onChangeItemsPerPage={onChangeItemsPerPage}
      />
    </>
  );
};

export default PokemonList;

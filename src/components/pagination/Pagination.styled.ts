import { styled } from '@mui/system';

export const PaginationStyled = styled('div')({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  gap: '20px',
  marginTop: '20px',
});

import React from 'react';
import {
  MenuItem,
  Pagination as MuiPagination,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import { PaginationStyled } from './Pagination.styled';

interface PaginationProps {
  totalCount: number;
  currentPage: number;
  itemsPerPage: number;
  onChangePage: (page: number) => void;
  onChangeItemsPerPage: (itemsPerPage: number) => void;
}

const Pagination: React.FC<PaginationProps> = ({
  totalCount,
  currentPage,
  itemsPerPage,
  onChangePage,
  onChangeItemsPerPage,
}) => {
  const totalPages = Math.ceil(totalCount / itemsPerPage);

  const handlePageChange = (
    event: React.ChangeEvent<unknown>,
    page: number
  ) => {
    onChangePage(page);
  };

  const handleItemsPerPageChange = (event: SelectChangeEvent<number>) => {
    const newItemsPerPage = Number(event.target.value) as number;
    onChangeItemsPerPage(newItemsPerPage);
  };

  return (
    <PaginationStyled>
      <Select
        value={itemsPerPage}
        onChange={handleItemsPerPageChange}
        variant="outlined"
      >
        <MenuItem value={10}>10</MenuItem>
        <MenuItem value={20}>20</MenuItem>
        <MenuItem value={50}>50</MenuItem>
      </Select>

      <MuiPagination
        count={totalPages}
        page={currentPage}
        onChange={handlePageChange}
      />
    </PaginationStyled>
  );
};

export default Pagination;

import React, { useEffect, useState } from 'react';
import { Backdrop, Modal } from '@mui/material';
import {
  ModalCloseButton,
  ModalContent,
  PokemonAbilityList,
  PokemonAbilityTitle,
  PokemonAttribute,
  PokemonImage,
  PokemonName,
  PokemonStatItem,
  PokemonStatList,
  PokemonStatTitle,
  PokemonType,
} from './PokemonModal.styled';
import { Pokemon } from '../pokemon-list/PokemonList';

interface PokemonModalProps {
  pokemon: Pokemon;
  onClose: () => void;
}

interface PokemonAPIResponse {
  id: number;
  name: string;
  height: number;
  weight: number;
  types: { slot: number; type: { name: string; url: string } }[];
  abilities: {
    slot: number;
    is_hidden: boolean;
    ability: { name: string; url: string };
  }[];
  stats: {
    base_stat: number;
    effort: number;
    stat: { name: string; url: string };
  }[];
  sprites: {
    back_default: string;
    back_shiny: string;
    front_default: string;
    front_shiny: string;
  };
}

const PokemonModal: React.FC<PokemonModalProps> = ({ pokemon, onClose }) => {
  const [pokemonInfo, setPokemonInfo] = useState<PokemonAPIResponse | null>(
    null
  );

  useEffect(() => {
    if (pokemon.id) {
      fetch(`https://pokeapi.co/api/v2/pokemon/${pokemon.id}`)
        .then((response) => response.json())
        .then((data: PokemonAPIResponse) => {
          console.log(data);
          const pokemonInfo: PokemonAPIResponse = {
            id: data.id,
            name: data.name,
            height: data.height,
            weight: data.weight,
            types: data.types,
            abilities: data.abilities,
            stats: data.stats,
            sprites: data.sprites,
          };

          setPokemonInfo(pokemonInfo);
        })
        .catch((error) => console.log(error));
    }
  }, [pokemon]);

  return (
    <Modal
      open={Boolean(pokemon)}
      onClose={onClose}
      BackdropComponent={Backdrop}
    >
      {pokemonInfo ? (
        <ModalContent>
          <ModalCloseButton onClick={onClose}>Close</ModalCloseButton>
          <PokemonName variant="h4">{pokemonInfo.name}</PokemonName>
          <PokemonImage
            src={pokemonInfo.sprites.front_default}
            alt={pokemonInfo.name}
          />
          <PokemonType variant="body1">
            Type: {pokemonInfo.types.map((type) => type.type.name).join(', ')}
          </PokemonType>
          <PokemonAttribute variant="body1">
            Height: {pokemonInfo.height} cm
          </PokemonAttribute>
          <PokemonAttribute variant="body1">
            Weight: {pokemonInfo.weight} kg
          </PokemonAttribute>
          <PokemonAbilityTitle variant="h5">Abilities:</PokemonAbilityTitle>
          <PokemonAbilityList>
            {pokemonInfo.abilities.map((ability, index) => (
              <li key={index}>{ability.ability.name}</li>
            ))}
          </PokemonAbilityList>
          <PokemonStatTitle variant="h5">Stats:</PokemonStatTitle>
          <PokemonStatList>
            {pokemonInfo.stats.map((stat, index: number) => (
              <PokemonStatItem key={index}>{stat.stat.name}</PokemonStatItem>
            ))}
          </PokemonStatList>
        </ModalContent>
      ) : (
        <div>Loading</div>
      )}
    </Modal>
  );
};

export default PokemonModal;

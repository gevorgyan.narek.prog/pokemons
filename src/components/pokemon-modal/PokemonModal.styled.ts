import styled from '@emotion/styled';
import { Button, Typography } from '@mui/material';

export const ModalContent = styled.div`
  background-color: #ffffff;
  border-radius: 8px;
  padding: 16px;
  max-width: 400px;
  text-align: center;
  box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.1);
  z-index: 100; /* Добавляем z-index, чтобы модальное окно было выше тени */
  position: relative; /* Добавляем position: relative, чтобы модальное окно было позиционировано относительно родительского контейнера */
  top: 50%;
  left: 50%;
  right: 50%;
  bottom: 50%;
  transform: translate(-50%, -50%);
`;
export const ModalCloseButton = styled(Button)`
  position: absolute;
  top: 8px;
  right: 8px;
  color: #757575;

  &:hover {
    color: #333333;
  }
`;

export const PokemonName = styled(Typography)`
  font-size: 24px;
  font-weight: bold;
  margin-bottom: 16px;
  color: #333333;
`;

export const PokemonImage = styled.img`
  width: 200px;
  height: 200px;
  object-fit: cover;
  margin-bottom: 16px;
  border-radius: 50%;
  box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.1);
`;

export const PokemonType = styled(Typography)`
  font-size: 16px;
  margin-bottom: 8px;
  color: #757575;
`;

export const PokemonAttribute = styled(Typography)`
  font-size: 14px;
  margin-bottom: 8px;
  color: #757575;
`;

export const PokemonAbilityTitle = styled(Typography)`
  font-size: 18px;
  font-weight: bold;
  margin-bottom: 8px;
  color: #333333;
`;

export const PokemonAbilityList = styled.ul`
  margin: 0;
  padding-left: 16px;
  text-align: left;
`;

export const PokemonStatTitle = styled(Typography)`
  font-size: 18px;
  font-weight: bold;
  margin-bottom: 8px;
  color: #333333;
`;

export const PokemonStatList = styled.ul`
  margin: 0;
  padding-left: 16px;
  text-align: left;
`;

export const PokemonStatItem = styled.li`
  list-style-type: none;
  margin-bottom: 4px;
`;
